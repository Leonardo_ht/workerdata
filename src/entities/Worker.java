package entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Worker {

	private String name;
	private WorkerLevel level;
	private Double baseSalary;
	
	private Department departmant;
	private List<HourContract> contracts = new ArrayList<HourContract>() ;
	
	public Worker() {
	}

	public Worker(String name, WorkerLevel level, Double baseSalary, Department departmant) {
		this.name = name;
		this.level = level;
		this.baseSalary = baseSalary;
		this.departmant = departmant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WorkerLevel getLevel() {
		return level;
	}

	public void setLevel(WorkerLevel level) {
		this.level = level;
	}

	public Double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(Double baseSalary) {
		this.baseSalary = baseSalary;
	}

	public Department getDepartmant() {
		return departmant;
	}

	public void setDepartmant(Department departmant) {
		this.departmant = departmant;
	}

	public List<HourContract> getContracts() {
		return contracts;
	}
	
	public void addContract(HourContract contract) {
		this.contracts.add(contract);
	}
	
	public void removeContract(HourContract contract) {
		this.contracts.remove(contract);
	}
	
	public double income(int year, int month) {
		double soma = this.baseSalary;
		Calendar cal = Calendar.getInstance();
		for (HourContract contract : contracts) {
			cal.setTime(contract.getDate());
			int contract_year = cal.get(Calendar.YEAR);
			int contract_month = cal.get(Calendar.MONTH);
			if (year == contract_year && month == contract_month) {
				soma += contract.totalValue();
			}
		}
		return soma;
	}
}
